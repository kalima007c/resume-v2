import React from 'react'
import {Container} from 'reactstrap'
import {Avatar,Divider,Col,Row,Timeline} from 'antd'
import '../style.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faJava ,faHtml5 , faCss3 , faReact , faSass ,faJs , faVuejs ,faPython ,faDocker,faGit,faLinux } from '@fortawesome/fontawesome-free-brands'


export default class Info extends React.Component {
    constructor(prop)
    {
        super(prop)
        this.state = {show:false};
        this.handleScroll();
    }
    handleScroll() {
        this.setState({ show: !this.state.show });
      }
    componentDidMount (){
       
    }
    render() {
        return (
            <Container className="info-font content-box">
            <div>
             
                    <ProfileImg/>
       
            </div>
            
 
            <div><About/></div>

            <div><Bio/></div>

            <div><ProgramingSkill/></div>

            <div><Education/></div>

            </Container>
        )
    }
}

const ProfileImg = () => {
    return (
        
          <Container>
            <div className="center info"><Avatar src="http://graph.facebook.com/100000337503807/picture?type=large" size={200}/></div>
            <div className="nameCap"><h2>Donnukrit Satirakul</h2></div>
          </Container>
        
    )
}
const About = () => {
    return(
        <div>
            <Divider>About Me</Divider>
            <div className="text-centered">
                <p>Hi! you can call me Phone. I'm 4th year student from Prince of Songkhla University Phuket Campus.</p>
                <p>I've been lived in Phuket for 4 years. I came from Nakorn Si Thammarat.</p>
                <p>Now I've gotten to practice programing skill such like Java , Javascript.</p>
            </div>
        </div>
    )
}
const Bio = () => {
    return (
        <Container>
            <Divider>My Info</Divider>
            <div>
                <p>Date of Birth : 1 January 1997</p>
                <p>Education : Faculty of Engineering (Computer Engineering)</p>
                <p>Major : Information Engineering</p>
                <p>Lifestyle : Xiaomi, Money, Coding, Reading, Photography</p>
                <p> Interesting 
                    <li>Web Development</li>
                    <li>Moblie App</li>
                    <li>Database Management</li>
                </p>
                <p>English Skill Level : C1</p>
                <p>Now Focus : React , Java Programing , Java Script , Python</p>
                <p>Next Focus : Back-End</p>
            </div>
        </Container>
    )
}
const ProgramingSkill = () => {
    return (
        <Container style={{paddingBottom:"5%"}}>
        <div>
            <Divider>Programing Skills</Divider>
        </div>
        <div>
            <Row gutter={8}>
                <Col span={12}>
                    <Divider>Intermediate</Divider>
                    <Container className="icon">
                        <FontAwesomeIcon icon={faJava}/>
                        <FontAwesomeIcon icon={faJs}/>
                        <FontAwesomeIcon icon={faReact}/>
                        <FontAwesomeIcon icon={faHtml5}/>
                        <FontAwesomeIcon icon={faCss3}/>
                        <FontAwesomeIcon icon={faSass}/>
                    </Container>
                </Col>
                <Col span={12}>
                    <Divider>Basic</Divider>
                    <Container className="icon">
                        <FontAwesomeIcon icon={faDocker}/>
                        <FontAwesomeIcon icon={faVuejs}/>
                        <FontAwesomeIcon icon={faPython}/>
                        <FontAwesomeIcon icon={faGit}/>  
                        <FontAwesomeIcon icon={faLinux}/>  
                    </Container>
                </Col>
            </Row>
        </div>
        </Container>
    )
}
const Education = () => {
    return(
        <div><Divider>Education</Divider>
            <div style={{paddingLeft:"10%"}}>
            <Timeline> 
                <Timeline.Item color="green">Pakphanang School</Timeline.Item>
                <Timeline.Item>Faculty of Engineering , Prince of Songkhla University</Timeline.Item>      
            </Timeline>
            </div>
        </div>
    )
}
