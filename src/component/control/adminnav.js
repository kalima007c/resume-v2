import React from 'react'
import { Layout, Menu} from 'antd';
const { Header } = Layout;

export default class Nav extends React.Component {
    render() {
        return(
            <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
            <Menu
                 theme="dark"
                 mode="horizontal"
                 defaultSelectedKeys={['1']}
                 style={{ lineHeight: '64px' }}
            >
                <Menu.Item key="1">Home</Menu.Item>
                <Menu.Item key="2">System Control</Menu.Item>
            </Menu>
            </Header>
        )
    }
}