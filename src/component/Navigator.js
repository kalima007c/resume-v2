import React from 'react'
import { Menu, Icon } from 'antd';
const { SubMenu } = Menu;

export default class Nav extends React.Component {
    state = {
        current: 'mail',
      };
    
      handleClick = e => {
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      };

    render() {
        return(
            <div>
            <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
                <Menu.Item key="home">
                    <Icon type="home" />
                        Home
                </Menu.Item>
            <SubMenu
                title={
                    <span className="submenu-title-wrapper">
                    <Icon type="appstore" />
                        My App
                    </span>
                }
                 >
                 <Menu.ItemGroup>
                    <Menu.Item key="setting:1"><a href="https://sso-comp-sim.web.app/">SSO Composition Simulator</a></Menu.Item>
                    <Menu.Item key="setting:2">Contact Calculator</Menu.Item>
                    <Menu.Item key="setting:3">Grade Prediction Calculator</Menu.Item>
                </Menu.ItemGroup>
               
            </SubMenu>
                
            <SubMenu
                title={
                    <span className="submenu-title-wrapper">
                    <Icon type="contacts" />
                        Contact
                    </span>
                }
            >
                 <Menu.ItemGroup title="Social Media">               
                    <Menu.Item key="setting:1"><a href="https://www.instagram.com/Iphone.coepk/">Instagram</a></Menu.Item>
                    <Menu.Item key="setting:2"><a href="https://medium.com/@dev.dnk">Medium</a></Menu.Item>
                </Menu.ItemGroup>
                <Menu.ItemGroup title="Repositories">
                    <Menu.Item key="setting:3"><a href="https://gitlab.com/kalima007c">Gitlab</a></Menu.Item>
                    <Menu.Item key="setting:4"><a href="https://github.com/KalimaPz">Github</a></Menu.Item>
                </Menu.ItemGroup>
        </SubMenu>
        
      </Menu>
            </div>
        )
    }
}