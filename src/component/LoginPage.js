import React from 'react'
import './login.css'
import auth from '../firebase'
import {Input,Button,Form} from 'antd'
import Administration from './control/admin'
export default class LoginPage extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
          email: '',
          password: '',
          message: '',
          currentUser: null
        }
      }
    
      componentDidMount() {
        auth.onAuthStateChanged(user => {
          if (user) {
            this.setState({
              currentUser: user
            })
          }
        })
      }
    
      onChange = e => {
        const { name, value } = e.target
    
        this.setState({
          [name]: value
        })
      }
    
      onSubmit = e => {
        e.preventDefault()
    
        const { email, password } = this.state
        auth
          .signInWithEmailAndPassword(email, password)
          .then(response => {
            this.setState({
              currentUser: response.user
            })
          })
          .catch(error => {
            this.setState({
              message: error.message
            })
            console.log('Wrong Email or Password')
          })
      }
    
      logout = e => {
        e.preventDefault()
        auth.signOut().then(response => {
          this.setState({
            currentUser: null
          })
        })
      }
    render(){
        const { currentUser } = this.state

    if (currentUser) {
      return (
       <div>
         <button onClick={this.logout}>Logout</button>
       <Administration
         email = {currentUser.email}
       />
       </div>
      )
    }
        return(
            <div className='main-font'>
                <div className='container-space-around header'>
                    Administrator Login
                </div>
                <div className='container-space-around'>
                    <div className='box-container'style={{textAlign:'center',padding:'5%'}}>
                    <Form onSubmit={this.onSubmit}>
                        <Form.Item>
                            E-mail
                            <Input
                            name = "email"
                            type = "email"
                            onChange = {this.onChange}
                            />
                        </Form.Item>
                        <Form.Item>
                            Password
                            <Input
                            name = "password"
                            type = "password"
                            onChange = {this.onChange}
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">Sign In</Button>
                        </Form.Item>
                    </Form>
                    </div>
                </div>
            </div>
        )
    }
}